package org.semeniv.downloadfiles;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.*;
import java.time.Duration;
import java.util.HashMap;

public class DownloadFiles {

    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        MyFileUtils.createDownloadDirectory();

        HashMap<String, Object> chromePrefs = new HashMap<>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory",
                new File(MyFileUtils.DirectoryFor.DOWNLOAD.getDirName()).getAbsolutePath());

        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @AfterClass
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @DataProvider(name = "data-amount-type")
    public Object[][] dataProvFunc() {
        return new Object[][]{
                {"10", "Sentences", "download/easyinfo.txt"},
                {"1", "Paragraphs", "download/easyinfo (1).txt"},
                {"70", "Words", "download/easyinfo (2).txt"},
        };
    }

    @Test(dataProvider = "data-amount-type")
    public void downFiles(String amountValue, String typeValue, String filePath) throws Exception {
        driver.get("https://www.webfx.com/tools/lorem-ipsum-generator/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        WebElement type = driver.findElement(By.xpath("//select[@class=\"form-control type-select\"]"));
        wait.until(ExpectedConditions.elementToBeClickable(type));
        type.sendKeys(typeValue);

        WebElement amount = driver.findElement(By.id("amount_generator"));
        amount.click();
        amount.clear();
        amount.sendKeys(amountValue);

        WebElement generateButton = driver.findElement(By.xpath("//input[@class=\"btn form-submit\"]"));
        generateButton.click();

        WebElement textArea = driver.findElement(By.xpath("//textarea[@name=\"result_field\"]"));

        String dataFromTextArea = textArea.getText();

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", textArea);

        driver.get("https://demo.seleniumeasy.com/generate-file-to-download-demo.html");
        WebElement enterData = driver.findElement(By.id("textbox"));
        enterData.click();

        enterData.sendKeys(dataFromTextArea);

        driver.findElement(By.id("create")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("link-to-download")));

        WebElement element = driver.findElement(By.id("link-to-download"));
        element.click();
        Thread.sleep(3000);

        Assert.assertEquals(MyFileUtils.readFileInString(String.valueOf(filePath)), dataFromTextArea);
    }
}